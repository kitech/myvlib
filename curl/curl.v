module curl

import vop

#flag -lcurl
#flag -I/usr/include

#include <curl/curl.h>

import const (
	CURLOPT_WRITEFUNCTION
	CURLOPT_SSL_VERIFYPEER
	CURLOPT_HEADERFUNCTION
	CURLOPT_WRITEDATA
	CURLOPT_HEADERDATA
	CURLOPT_FOLLOWLOCATION
	CURLOPT_URL
	CURLOPT_VERBOSE
	CURLOPT_HTTP_VERSION
	CURL_HTTP_VERSION_1_1
	CURLOPT_HTTPHEADER
	CURLOPT_POSTFIELDS
	CURLOPT_CUSTOMREQUEST
	CURLOPT_TCP_KEEPALIVE
	CURLINFO_CONTENT_LENGTH_DOWNLOAD 
	CURLE_OK
)

const (
	OPT_WRITEFUNCTION							 =	CURLOPT_WRITEFUNCTION						
	OPT_SSL_VERIFYPEER						 =	CURLOPT_SSL_VERIFYPEER					
	OPT_HEADERFUNCTION						 =	CURLOPT_HEADERFUNCTION					
	OPT_WRITEDATA									 =	CURLOPT_WRITEDATA								
	OPT_HEADERDATA								 =	CURLOPT_HEADERDATA							
	OPT_FOLLOWLOCATION						 =	CURLOPT_FOLLOWLOCATION					
	OPT_URL												 =	CURLOPT_URL											
	OPT_VERBOSE										 =	CURLOPT_VERBOSE									
	OPT_HTTP_VERSION							 =	CURLOPT_HTTP_VERSION						
	_HTTP_VERSION_1_1							 =	CURL_HTTP_VERSION_1_1						
	OPT_HTTPHEADER								 =	CURLOPT_HTTPHEADER							
	OPT_POSTFIELDS								 =	CURLOPT_POSTFIELDS							
	OPT_CUSTOMREQUEST							 =	CURLOPT_CUSTOMREQUEST						
	OPT_TCP_KEEPALIVE							 =	CURLOPT_TCP_KEEPALIVE						
	INFO_CONTENT_LENGTH_DOWNLOAD	 =	CURLINFO_CONTENT_LENGTH_DOWNLOAD
	E_OK													 =	CURLE_OK												
)

struct C.CURL{}

struct Curl {
mut:
   h *C.CURL
   fa fn()
   scc string
   cknum int
   rv int
   err string
}

struct Slist {
mut:
  lst voidptr // *C.curl_slist
}

fn C.curl_easy_init() *C.CURL
fn C.curl_easy_perform(h *C.CURL) int
fn C.curl_easy_cleanup(h *C.CURL)
fn C.curl_easy_setopt(h *C.CURL, opt int, val u64) int
fn C.curl_easy_strerror(eno int) byteptr
fn C.curl_slist_append(l voidptr, s byteptr) voidptr
fn C.curl_slist_free_all(l voidptr)

fn new() *Curl {
   mut o := &Curl{}
   o.h = C.curl_easy_init()
   o.scc = ''
   o.cknum = 0
   return o
}
fn (ch mut Curl) clean() {
   if ch == vop.nilptr || ch.h == vop.nilptr { return }
   C.curl_easy_cleanup(ch.h)
   ch.h = vop.nilptr
}

fn (ch mut Curl) writecb(ptr byteptr, size i64, nmemb i64, userp voidptr) i64{
  ch.cknum += 1
  ch.scc = ch.scc + tos(ptr, int(nmemb))

  return nmemb
}

fn writecb(ptr byteptr, size i64, nmemb i64, userp *Curl) i64 {
  mut ch := userp
  return ch.writecb(ptr, size, nmemb, 0)
}

fn (ch mut Curl) perform() ?string {
  C.curl_easy_setopt(ch.h, OPT_WRITEFUNCTION, writecb)
  C.curl_easy_setopt(ch.h, OPT_WRITEDATA, ch)

  rv := C.curl_easy_perform(ch.h)
  ch.rv = rv
  ch.err = ch.err + tos2(C.curl_easy_strerror(rv))

  if rv == E_OK {
    return ch.scc
  }
  return error(ch.err)
}

// fn (ch mut Curl) setopt<T>(opt int, val T) {
//    rv := C.curl_easy_setopt(ch.h, opt, val)
//    println(rv.str() + 'setopti')
// }

fn (ch mut Curl) setopti(opt int, val int) {
  rv := C.curl_easy_setopt(ch.h, opt, val)
  println(rv.str() + 'setopti')
}

fn (ch mut Curl) setoptb(opt int, val bool) {
   rv := C.curl_easy_setopt(ch.h, opt, val)
   println(rv.str() + 'setoptb')
}

fn (ch mut Curl) setopto(opt int, val voidptr) {
   rv := C.curl_easy_setopt(ch.h, opt, val)
   println(rv.str() + 'setoptp')
}

fn (ch mut Curl) setopts(opt int, val string) {
   rv := C.curl_easy_setopt(ch.h, opt, val)
   println(rv.str() + 'setopts')
}

fn (ch mut Curl) setoptl(opt int, val Slist) {
   rv := C.curl_easy_setopt(ch.h, opt, val.lst)
   println(rv.str() + 'setoptl')
}

fn newslist() *Slist {
   mut o := &Slist{}
   o.lst = vop.nilptr
   return o
}

fn (l mut Slist) append(s string) {
   l.lst = C.curl_slist_append(l.lst, s.str)
}
fn (l mut Slist) free() {
  C.curl_slist_free_all(l.lst)
  l.lst = vop.nilptr
}

fn hello() {
  println('heheh')
  h := C.curl_easy_init()
  println('heheh')
  println(h)
}

